
$('#c_fname').on('keyup',function()
{
	//alert(name);
	val =$(this).val();
	if(val.match(/^[a-zA-Z]+$/))
	{
		$('#errorc_fname').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorc_fname').show();
		$('#errorc_fname').html('Enter valid  Name');
		$('#errorc_fname').css('color','red');
	}
});	

$('#c_lname').on('keyup',function()
{
	//alert(name);
	val =$(this).val();
	if(val.match(/^[a-zA-Z]+$/))
	{
		$('#errorc_lname').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorc_lname').show();
		$('#errorc_lname').html('Enter valid Last Name');
		$('#errorc_lname').css('color','red');
	}
});	



$('#c_email').on('change',function()
{
	val =$(this).val();
	if(val.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i))
	{
		$('#errorc_email').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorc_email').show();
		$('#errorc_email').html('Enter valid Email');
		$('#errorc_email').css('color','red');
	}
});

$('#city').on('blur',function()
{
	val =$(this).val();
	// console.log(val);
	if(val != -1)
	{
		$('#errorcity').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorcity').show();
		$('#errorcity').html('Select City');
		$('#errorcity').css('color','red');
	}
});
$('#c_mobile').on('keyup',function()
{
	val =$(this).val();
	if(val.match(/^[7/8/9][0-9]{9}$/))
	{
		$('#errorc_mobile').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorc_mobile').show();
		$('#errorc_mobile').html('Enter valid Mobile');
		$('#errorc_mobile').css('color','red');
	}
});


$('#contact').on('keyup',function()
{
	val =$(this).val();
	if(val.match(/^[7/8/9][0-9]{9}$/))
	{
		$('#errorcontact').hide();
		$(this).css('border-color','');
	}
	else
	{
		$(this).css('border-color','red');
		$('#errorcontact').show();
		$('#errorcontact').html('Enter valid Contact No');
		$('#errorcontact').css('color','red');
	}
});