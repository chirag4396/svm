<?php
 include ("config.php"); 
 ?>
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Charity</title>
    <meta name="description" content="Free Bootstrap Theme by uicookies.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Montserrat:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/styles-merged.css">
    <link rel="stylesheet" href="css/style.min.css">
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    
      <nav class="navbar navbar-default probootstrap-navbar">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html" title="uiCookies:Enlight">Enlight</a>
          </div>

          <div id="navbar-collapse" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
        <li ><a href="index.html">Home</a></li>
              <li><a href="about.html">About Us</a></li>
              <li><a href="causes.html">Our Work</a></li>
              <li class="active"><a href="gallery.php">Gallery</a></li>
                  <li><a href="contact.php">Contact us</a></li>
            <!--   <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Pages</a>
                <ul class="dropdown-menu">
                  <li><a href="about.html">About Us</a></li>
                  <li><a href="testimonial.html">Testimonial</a></li>
                  <li><a href="cause-single.html">Cause Single</a></li>
                  <li><a href="gallery.html">Gallery</a></li>
                  <li class="dropdown-submenu dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><span>Sub Menu</span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Second Level Menu</a></li>
                      <li><a href="#">Second Level Menu</a></li>
                      <li><a href="#">Second Level Menu</a></li>
                      <li><a href="#">Second Level Menu</a></li>
                    </ul>
                  </li>
                  <li><a href="news.html">News</a></li>
                  <li><a href="contact.html">Contact</a></li>
                </ul>
              </li> -->
              
              <li class="probootstra-cta-button last"><a href="contact.php" class="btn btn-primary">Donate</a></li>
            </ul>
          </div>
        </div>
      </nav>
      
      <section class="probootstrap-hero probootstrap-hero-inner" style="background-image: url(img/hero_bg_bw_1.jpg)"  data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="probootstrap-slider-text probootstrap-animate" data-animate-effect="fadeIn">
                <h1 class="probootstrap-heading probootstrap-animate">Our Gallery <span>Humanity is our religion.</span></h1>
              </div>
            </div>
          </div>
        </div>
      </section>

<section class="probootstrap-section">
<?php 

                  $res = $conn->query('select * from gallary');
                  if($res->num_rows){
                    
                    while ($row = $res->fetch_assoc()) {

                     
                      ?>
        <div class="container">
         
          <div class="row probootstrap-gutter10">
           
            <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate fadeInUp probootstrap-animated">
              <a href="<?php echo 'admin/'.$row['gal_image'];?>" class="image-popup"><img src="<?php echo 'admin/'.$row['gal_image'];?>"  class="img-responsive"></a>
            </div>
           <!--  <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate fadeInUp probootstrap-animated">
              <a href="img/img_sq_2.jpg" class="image-popup"><img src="img/img_sq_2.jpg"  class="img-responsive"></a>
            </div> -->
           <!--  <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate fadeInUp probootstrap-animated">
              <a href="img/img_sq_3.jpg" class="image-popup"><img src="img/img_sq_3.jpg"  class="img-responsive"></a>
            </div> -->
            <!-- <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate fadeInUp probootstrap-animated">
              <a href="img/img_sq_4.jpg" class="image-popup"><img src="img/img_sq_4.jpg"  class="img-responsive"></a>
            </div> -->
           <!--  <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate fadeInUp probootstrap-animated">
              <a href="img/img_sq_5.jpg" class="image-popup"><img src="img/img_sq_5.jpg"  class="img-responsive"></a>
            </div> -->
           <!--  <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate fadeInUp probootstrap-animated">
              <a href="img/img_sq_6.jpg" class="image-popup"><img src="img/img_sq_6.jpg"  class="img-responsive"></a>
            </div> -->
           <!--  <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate fadeInUp probootstrap-animated">
              <a href="img/img_sq_7.jpg" class="image-popup"><img src="img/img_sq_7.jpg"  class="img-responsive"></a>
            </div> -->
           <!--  <div class="col-md-3 col-sm-4 col-xs-6 gal-item probootstrap-animate fadeInUp probootstrap-animated">
              <a href="img/img_sq_1.jpg" class="image-popup"><img src="img/img_sq_1.jpg"  class="img-responsive"></a>
            </div> -->
          
           <?php } } ?>
          </div>
           
        </div>

      </section>
   
<footer class="probootstrap-footer probootstrap-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-8 probootstrap-animate">
              <div class="probootstrap-footer-widget">
                <h3>About Us</h3>
                <p>Sarvajanik Vikas Manch is a non-governmental organization striving since 2012 to usher in development of the society.
Our Foundation is blessed to have people who have a strong inclination towards social work and have the desire to do something for the underprivileged people. 
</p>
                <ul class="probootstrap-footer-social">
            
                  <li><a href="#"><i class="icon-facebook"></i></a></li>
               
                  <li><a href="#"><i class="icon-youtube"></i></a></li>
                </ul>
              </div>
            </div>
           
            <div class="col-md-4 probootstrap-animate">
              <div class="probootstrap-footer-widget">
                <h3>Contact Info</h3>
                <ul class="probootstrap-contact-info">
                  <li>
                    <span>Jay Bhavani Chowk,</span> </br>
                    <span>Srinagar, Rahtani</span></br>
                    <span>Kalewadi Link Road,Pune</span></br>
                   
                  </li>
                  <li><i class="icon-mail"></i><span>svmpunegroup@gmail.com</span></li>
                  <li><i class="icon-phone2"></i><span>9146027165 , 9860203635</span></li>
                </ul>
                
              </div>
            </div>

           
          </div>
          <!-- END row -->
          
        </div>

        <div class="probootstrap-copyright">
          <div class="container">
            <div class="row">
              <div class="col-md-8 text-left">
                <p>&copy; 2018 <a href="https://sungare.com/">sungare technologies</a>. All Rights Reserved.  </p>
              </div>
              <div class="col-md-4 probootstrap-back-to-top">
                <p><a href="#" class="js-backtotop">Back to top <i class="icon-arrow-long-up"></i></a></p>
              </div>
            </div>
          </div>
        </div>
      </footer>

    <script src="js/scripts.min.js"></script>
    <script src="js/main.min.js"></script>
    <script src="js/custom.js"></script>


  </body>
</html>