<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Charity</title>
	<meta name="description" content="Free Bootstrap Theme by uicookies.com">
	<meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Montserrat:300,400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="css/styles-merged.css">
	<link rel="stylesheet" href="css/style.min.css">
	<link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
      <![endif]-->
  </head>
  <body>


  	<nav class="navbar navbar-default probootstrap-navbar">
  		<div class="container">
  			<div class="navbar-header">
  				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
  					<span class="sr-only">Toggle navigation</span>
  					<span class="icon-bar"></span>
  					<span class="icon-bar"></span>
  					<span class="icon-bar"></span>
  				</button>
  				<a class="navbar-brand" href="index.html" title="uiCookies:Enlight">Enlight</a>
  			</div>

  			<div id="navbar-collapse" class="navbar-collapse collapse">
  				<ul class="nav navbar-nav navbar-right">
  					<li ><a href="index.html">Home</a></li>
  					<li><a href="about.html">About Us</a></li>
  					<li><a href="causes.html">Our Work</a></li>
  					<li><a href="gallery.php">Gallery</a></li>
  					<li class="active"><a href="contact.php">Contact us</a></li>
            
            <li class="probootstra-cta-button last"><a href="contact.php" class="btn btn-primary">Donate</a></li>
        </ul>
    </div>
</div>
</nav>

<section class="probootstrap-hero probootstrap-hero-inner" style="background-image: url(img/hero_bg_bw_1.jpg)"  data-stellar-background-ratio="0.5">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="probootstrap-slider-text probootstrap-animate" data-animate-effect="fadeIn">
					<h1 class="probootstrap-heading probootstrap-animate">Contact us <span>We look forward to hearing from you.</span></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="probootstrap-section">
	<div class="container">
		<div class="row">
			<div class="col-md-5 probootstrap-animate fadeInUp probootstrap-animated br">
				<form action="#" name="frmcontact" id="frmcontact" method="post" class="probootstrap-form">
					<div class="form-group">
						<label for="name">Full Name</label>
						<input type="text" class="form-control" id="c_fname" name="c_fname"  required autocomplete="off" autosave="off">  
						<span id="errorc_fname" class="error-span"></span>
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control" id="c_email" name="c_email"  required autocomplete="off" autosave="off">  
						<span id="errorc_email" class="error-span"></span>
					</div>
<div class="form-group">
						<label for="email">mobile Number</label>
						<input type="mobile" class="form-control" id="c_mobile" name="c_mobile"  required autocomplete="off" autosave="off">  
						<span id="errorc_mobile" class="error-span"></span>
					</div>
					<div class="form-group">
						<label for="message">Message</label>
						<textarea cols="30" rows="10" class="form-control" id="c_message" name="c_message"></textarea>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-primary btn-lg" id="btnadd" name="btnadd" value="Send Message">
					</div>
					<center><font color="black"><div id="success_message" style="display:none;">Data Submitted Sucessfully </div></font></center>
				</form>
			</div>
			<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
			<script type="text/javascript" src="js/custom/myjquery.js"></script>
			<script src="js/contact.js"></script>

			<div class="col-md-6 col-md-push-1 probootstrap-animate fadeInUp probootstrap-animated">


				<h4>Address</h4>
				<ul class="probootstrap-contact-info">
					<li><i class="icon-pin"></i> <span>Jay Bhavani Chowk,
						Srinagar, Rahtani
						Kalewadi Link Road,Pune</span></li>
						<li><i class="icon-email"></i><span>  svmpunegroup@gmail.com</span></li>
						<li><i class="icon-phone"></i><span>  9146027165 , 9860203635</span></li>
					</ul>

					<img  src="img/m1.jpg">


				</div>
			</div>
		</div>
	</section>

	<footer class="probootstrap-footer probootstrap-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 probootstrap-animate">
					<div class="probootstrap-footer-widget">
						<h3>About Us</h3>
						<p>Sarajanik Vikas Manch is a non-governmental organization striving since 2012 to usher in development of the society.
							Our Foundation is blessed to have people who have a strong inclination towards social work and have the desire to do something for the underprivileged people. 
						</p>
						<ul class="probootstrap-footer-social">

							<li><a href="#"><i class="icon-facebook"></i></a></li>

							<li><a href="#"><i class="icon-youtube"></i></a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-4 probootstrap-animate">
					<div class="probootstrap-footer-widget">
						<h3>Contact Info</h3>
						<ul class="probootstrap-contact-info">
							<li>
								<span>Jay Bhavani Chowk,</span> </br>
								<span>Srinagar, Rahtani</span></br>
								<span>Kalewadi Link Road,Pune</span></br>

							</li>
							<li><i class="icon-mail"></i><span>svmpunegroup@gmail.com</span></li>
							<li><i class="icon-phone2"></i><span>9146027165 , 9860203635</span></li>
						</ul>

					</div>
				</div>


			</div>
			<!-- END row -->

		</div>

		<div class="probootstrap-copyright">
			<div class="container">
				<div class="row">
					<div class="col-md-8 text-left">
						<p>&copy; 2018 <a href="https://sungare.com/">sungare technologies</a>. All Rights Reserved.  </p>
					</div>
					<div class="col-md-4 probootstrap-back-to-top">
						<p><a href="#" class="js-backtotop">Back to top <i class="icon-arrow-long-up"></i></a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<script src="js/scripts.min.js"></script>
	<script src="js/main.min.js"></script>
	<script src="js/custom.js"></script>


</body>
</html>